package com.codewithd.product.service;

import com.codewithd.product.model.Product;
import com.codewithd.product.repo.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    private ProductRepository repository;

    @Autowired
    public ProductService(ProductRepository repository) {
        this.repository = repository;
    }

    public List<Product> getAll() {
        return repository.findAll();
    }

    public Product create(Product product) {
        return repository.save(product);
    }

    public Optional<Product> getProduct(String id) {
        return repository.findAll().stream()
                .filter(pr -> id.equals(pr.getId()))
                .map(Optional::of)
                .findAny()
                .orElseGet(Optional::empty);
    }

}
